import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../_services/user.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { User } from '../_models/User';

@Component({
    selector: 'user-edit',
    templateUrl: 'user-edit.component.html',
    styleUrls: ['user-edit.component.css']
})
export class UserEditComponent implements OnInit {
    userEditForm: FormGroup;
    showKey: boolean = false;
    id: number = null;
    // username: string = '';
    // password: string = '';
    // firstName: string = '';
    // lastName: string = '';
    // token: string = ''

    constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private formBuilder: FormBuilder) { }
    ngOnInit() {
        this.getUserById(this.route.snapshot.params['id']);
        this.userEditForm = this.formBuilder.group({
            'username': [null, Validators.required],
            'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            'firstName': [null, Validators.required],
            'lastName': [null, Validators.required],
        })
    }

    getUserById(userId) {
        this.userService.getById(userId).subscribe(data => {
            this.id = data.id
            this.userEditForm.setValue({
                username: data.username,
                password: data.password,
                firstName: data.firstName,
                lastName: data.lastName
            });
            console.log('this.userEditForm===', this.userEditForm)
        })
    }

    showHidePassword() {
        this.showKey = !this.showKey;
    }

    onFormSubmit(form: User) {
        if (this.userEditForm.invalid) {
            return;
        }
        let userId = { id: this.id }
        form = { ...form, ...userId }
        this.userService.update(form, this.id).subscribe(updatedResp => {
            this.router.navigate(['/']);
        }, err => {
            console.log('error', err)
        })
    }

}