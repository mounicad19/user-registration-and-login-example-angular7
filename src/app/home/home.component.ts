import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { User } from '../_models/User';
import { AuthenticationService } from '../_services/authentication.service';
import { UserService } from '../_services/user.service';
import { MatTableDataSource } from "@angular/material";
// import { ActivatedRoute, Router } from '@angular/router'

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    userTableDataSource = new MatTableDataSource(this.users);
    displayedColumns: string[] = ['Index', 'Username', 'firstName', 'lastName', 'Actions', 'Actions Edit'];

    constructor(private authenticationService: AuthenticationService, private userService: UserService,
     ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        })
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    ngOnDestroy() {
        this.currentUserSubscription.unsubscribe();
    }

    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers();
        })
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
            console.log('users==', this.users)
        });
    }

}