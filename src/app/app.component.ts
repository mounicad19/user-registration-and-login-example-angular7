import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertComponent } from './_components/alert.component';
import { AuthenticationService } from './_services/authentication.service';
import { User } from './_models/user';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  currentUser: User;

  constructor(private authService: AuthenticationService, private router: Router) {
    this.authService.currentUser.subscribe(userData => {
      this.currentUser = userData;
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login'])
  }

}

