import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators'

import { User } from '../_models/user';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>('/users')
    }

    getById(id: number) {
        return this.http.get<User>(`/users/${id}`)
    }

    register(user: User) {
        return this.http.post<User>('/users/register', user);
    }

    update(user: User, id: number) {
        return this.http.put<User>(`/users/${id}`, user).pipe(
            tap(_ => console.log(`updated user  ${id}`))
        )
    }

    delete(id: number) {
        return this.http.delete<User>(`/users/${id}`)
    }
}